# L6_GitExercise

Lesson 6 Exercise Task

3088 PiHat Project Description 

We are designing a multi-sensor microPI HAT module (including a telemetry sensor
made up of accelerometers, gyroscopes and magnetometers) that will be attached to
a Raspberry Pi Zero and powered externally for a range of potential applications.
These include: velocity and acceleration tracking for remote controlled ground
vehicles (such as RC cars), gyroscopic stabilization for quadcopter drones during
flight and remotely searching for faults in heavy current electrical systems in hard to
reach places by sensing irregularities in the expected magnetic field in an area.

Requirements:
● microPI HATs must follow all of the standard electrical HAT rules as laid out
for normal HATs, the only difference being that they have a smaller
mechanical form factor as seen in the diagram below
● Must conform to the basic add-on requirements of a HAT
● Must have a full size 40W GPIO connector
● Must follow the HAT mechanical specifications(https://github.com/raspberrypi/hats/blob/master/uhat-board-mechanical.pdf)
● Must use spacers 8mm or larger
● Must be able to supply 2A continuously to the PI
● Must have a correctly implemented ZVD’ safety circuit
